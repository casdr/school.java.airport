/*
 * Copyright (C) 2016 casdr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package me.casdr.airport;

import java.time.LocalTime;
import java.util.Date;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author casdr
 */
public class Flight extends Thread {

    public String code;
    public Lane lane;
    public LocalTime eta;
    public enum State {
        requested, assigned, landing, landed
    };
    public State state;
    public Random r;
    public Integer flightTime;

    public Flight() {
        state = State.requested;
        r = new Random();
        code = generateName();
        start();
    }

    public void run() {
        Boolean running = true;
        while (running) {
            switch (state) {
                case requested: {
                    // Some random timeout so it looks like it's real
                    try {
                        Thread.sleep((r.nextInt(5) + 1) * 1000);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(Flight.class.getName()).log(
                                Level.SEVERE, null, ex);
                    }
                    // Request a lane
                    requestLane();
                }
                case assigned: {
                    try {
                        // Some random timeout so it looks like it's real
                        Thread.sleep((r.nextInt(10) + 5) * 1000);
                        // Set the state to landing
                        state = State.landing;
                        Thread.sleep(flightTime * 1000);
                        // Set the state to landing
                        state = State.landed;
                        lane.occupied = false;
                    } catch (InterruptedException ex) {
                        Logger.getLogger(Flight.class.getName()).log(
                                Level.SEVERE, null, ex);
                    }
                }
                case landed: {
                    // Stop the loop!
                    running = false;
                    try {
                        // Let the visitors watch the plane for a few seconds
                        Thread.sleep(5 * 1000);
                        // Let the flight magically disappear
                        AirPort.flights.remove(this);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(Flight.class.getName()).log(
                                Level.SEVERE, null, ex);
                    }
                }
            }
        }
    }

    private void requestLane() {
        Boolean found = false;
        while (!found) {
            try {
                // Randomize the timeout so it looks like a real planes
                Thread.sleep((new Random().nextInt(5) + 5) * 1000);
            } catch (InterruptedException ex) {
                // Huh
                Logger.getLogger(Flight.class.getName()).log(
                        Level.SEVERE, null, ex);
            }
            if (AirPort.flights.indexOf(this) > AirPort.lanes.size() - 1) {
                // Hey, don't jump the queue!
                continue;
            }
            for (Lane l : AirPort.lanes) {
                // Let's see, is this lane free?
                if (l.requestLanding()) {
                    // Yay it is! Let's update my information
                    found = true;
                    lane = l;
                    state = State.assigned;
                    // Randomize the landing time
                    flightTime = r.nextInt(20) + 30;
                    eta = LocalTime.now().plusSeconds(Long.valueOf(flightTime));
                    break;
                }
            }
        }
    }

    private String generateName() {
        char first = (char) (r.nextInt(26) + 'a');
        char second = (char) (r.nextInt(26) + 'a');
        int number = r.nextInt(1000) + 8999;
        return (String.valueOf(first) + String.valueOf(second)
                + Integer.toString(number)).toUpperCase();
    }
}
