/*
 * Copyright (C) 2016 casdr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package me.casdr.airport;

import java.util.Date;

/**
 * Lane model
 * @author casdr
 */
public class Lane {
    public String code;
    public Boolean occupied = false;
    
    public Lane(String _code) {
        code = _code;
    }
    
    public Boolean requestLanding() {
        // Check if the lane is occupied
        if(occupied) {
            // I'm really sorry but I'm occupied by another plane :(
            return false;
        }
        // Set the occupied state to true
        occupied = true;
        // Sure, you can land here my friend!
        return true;
    }
}
