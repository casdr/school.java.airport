/*
 * Copyright (C) 2016 casdr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package me.casdr.airport;

import java.awt.BorderLayout;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

/**
 *
 * @author casdr
 */
public class Frame extends Thread {

    JFrame frame = new JFrame();
    JTable table = new JTable();
    String[] columns = {"Flight", "Status", "Lane", "ETA"};   

    
    // Let's build a frame!
    public Frame() {
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);     

        JScrollPane scrollPane = new JScrollPane(table);
        frame.add(scrollPane, BorderLayout.CENTER);
        frame.setSize(600, 210);
        frame.setVisible(true);

        start();

    }

    public void run() {
        // Do a loop
        while (true) {
            try {
                List<String[]> values = new ArrayList<String[]>();
                for (Flight f : AirPort.flights) {
                    // Default values
                    String eta = "-", status = "-", lane = "-";
                    if (f.state != null) {
                        String state = f.state.toString();
                        status = state.substring(0, 1).toUpperCase() +
                                state.substring(1);
                    }
                    if (f.eta != null) {
                        eta = f.eta.toString();
                    }
                    if (f.lane != null) {
                        lane = f.lane.code;
                    }
                    // Add the flight to the array
                    values.add(new String[]{f.code, status, lane, eta});
                }
                // Generate the table layout
                TableModel tableModel = new DefaultTableModel(
                        values.toArray(new Object[][]{}), columns);
                table.setModel(tableModel);
                // Refresh the table
                table.repaint();
                // We don't wanna use 100% CPU, do we? ;)
                Thread.sleep(1000);

            } catch (Exception ex) {
                // Haha
            }
        }

    }

}
