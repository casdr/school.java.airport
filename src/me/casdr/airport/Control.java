/*
 * Copyright (C) 2016 casdr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package me.casdr.airport;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Air Control
 * @author casdr
 */
public class Control extends Thread {

    public Random r;
    public Integer times;

    public static void generateFlight() {
        AirPort.flights.add(new Flight());
    }

    public Control() {
        r = new Random();
        times = 0;
        start();
    }

    public void run() {
        while (true) {
            // Generate a new flight if the amount of flights is lower than
            // the amount of lanes * 6. Randomized.
            if (AirPort.flights.size() < r.nextInt(AirPort.lanes.size() * 6)) {
                generateFlight();
                // No integer overflow, you never know how long this
                // application might be running.
                if(times < AirPort.lanes.size() * 7) {
                    times++;
                }
            }
            try {
                if (times < AirPort.lanes.size() * 5) {
                    // Initialize the planes with a small delay
                    Thread.sleep((r.nextInt(5)) * 100);
                } else {
                    // Some timeout for newer planes
                    Thread.sleep((r.nextInt(10) + 2) * 1000);
                }
            } catch (InterruptedException ex) {
                // Meh, just try it again
                Logger.getLogger(Control.class.getName()).log(Level.SEVERE,
                        null, ex);
            }
        }
    }
}
